#!/bin/bash

#
# @TODO : tester la copie des vidéos. je en suis pas sûr que la copie soit faite
# @TODO : prendre en compte la date de création du fichier si on a pas la date exif
# @TODO : ne pas laisser exiftool faire les copies des fichiers lorsque la sortie est spécifiée car il ne fait pas la copie lorsque la condition échoue.
# Algo :
#    # fonctionnement normal
#    if ( date_from_exif ) 
#        then date = date_from_exif
#    elif ( ! date_from_file_or_directory )
#        then date = date_from_file_creation
#    elif ( date_from_file_or_directory )
#        then date = date_from_file_creation
#
#    # force date_from_file_or_directory
#    if ( ! date_from_file_or_directory )
#        then date = date_from_file_creation
#    elif ( date_from_file_or_directory )
#        then date = date_from_file_creation


usage=$(basename $0)" -[ryhd] SRC_DIR [DST_DIR]
    This script prepare pictures and videos files in order to upload them into Google Photos.
    To do so, it:
    1/ looks for pictures and videos
    2/ detect the date of the picture according to the filename or directory name
    3/ add the date into the exif database, if missing.
    4/ change the modification date to the new determined date if the exif date is not present
        or for none jpeg file and whatever the status of the update of the exif date.
        This modification date is useful to help Google photo to classify none jpeg files or file without exif data.
    5/ extract comment from filename or parent directory and store it into the exif database and/or filename.
    
    DETERMINING THE DATE:
    The date is determined according to the name of the file or the folder and parent folder.
    Supported structures :
        - prefixYYYY-MM-DD_HH-MM-SSsuffix.jpg
        - prefixYYYY-MM-DD HH.MM.SSsuffix.jpg
        - prefixYYYY_MM_DDsuffix.jpg
        - prefixYYYYsuffix.jpg
        - prefixYYYYMMDD_HHMMSSsuffix.jpg
        - prefixDD-MM-YYYY HH-MM-SSsuffix.jpg
        - prefixYYYY-MM-DDsuffix/file.ext
        - prefixYYYYsuffix/file.ext
        - YYYY/MM/file.ext
    '-' can also be '.' or '_' or ' '
    The script is looking at least for the year. In that case, the default value is: YYYY:01:01 01:01:01


    OPTIONS:
    -r : recursive mode (always ON, can't change)
    -d : debug mode. Do nothing but display command and errors
    -h : help
    -y : Don't ask for confirmation
    -u : Update mode for exif data.
        - 'ALL': Update date and comment if exif datetimeoriginal is not present
        - 'FORCEALL': Force Exif update even if exif datetimeoriginal is present
        - 'COMMENT':Update ONLY the Exif comment. But this option still need the date to be detected into filename or directory
            If a comment is already set, it will be overwritten.
    -p : Rename Photo files: 'YYYY-MM-DD_HH-MM-SS.ext'
    -v : Rename Video files: 'YYYY-MM-DD_HH-MM-SS.ext'
    -c : Add comment to files: 'YYYY-MM-DD_HH-MM-SS COMMENT.ext'
        - 'ALL': Add comment only to any files
        - 'VIDEO': Add comment only to videos
        - 'PHOTO': Add comment only to photos

    <SRC_DIR> : target directory containing pictures to update
    <DST_DIR> : target directory to create a copy of the original pictures with exif data. 
        - If empty, original pictures are updated in-place.
    
    REMARKS:
    * This script uses exiftool that can be found there http://exiftool.org/
    * Supported files for exif : JPG, PNG, MP4, MOV, TIF, TIFF
    * Supported files for modification date : the previous + AVI
    * Files with exif data are skiped
    * Files that already exist into the output directory are not copied
    * Files without exif and without new determined date are not copied
    * outut log are value tab separated with header
    * carrefull, on fat32 file system, the range for modification date start from 1980-01-01, you cannot set modification date before
" 
recursive=""
autoconfirm=""
forceexifupdate=""
updatecommentonly=""
isvideo=""
renamephoto=""
renamevideo=""
addcomment=""
debug=
terminal="\n"
# test for exiftool success
re_success_exiftool="1 image files (created|updated)"
re_uptodate_exiftool="1 files failed condition"

while getopts "rhdyu:pvc:" option ; do
    case $option in
        h)
            echo "usage : ${usage}"
            exit
            ;;
        r)
            recursive="-r"
            ;;
        d)
            debug="-d"
            ;;
        y)
            autoconfirm="-y"
            ;;
        u)
            updatemode="${OPTARG}"
            ;;
        p)
            renamephoto="-p"
            ;;
        v)
            renamevideo="-v"
            ;;
        c)
            addcomment="${OPTARG}"
            ;;
    esac
done

shift $((OPTIND -1))


if [[ -z "$updatemode" || ( "$updatemode" != "ALL" && "$updatemode" != "FORCEALL" && "$updatemode" != "COMMENT" )  ]] ; then
    echo "The update mode is wrong (${updatemode})"
    echo "usage : ${usage}"
    exit
elif [[ "$updatemode" == "FORCEALL" ]] ; then
    forceexifupdate="$updatemode"
elif [[ "$updatemode" == "COMMENT" ]] ; then
    updatecommentonly="$updatemode"
fi

if [[ ! -z "${addcomment}" && "${addcomment}" != "ALL" && "${addcomment}" != "VIDEO" && "${addcomment}" != "PHOTO"  ]] ; then
    echo "The add comment parameter '-c' is wrong (${addcomment})"
    echo "usage : ${usage}"
    exit
fi

if [[ -z "$1" ]] ; then
    echo "The source directory is missing"
    echo "usage : ${usage}"
    exit
fi

if [[ ! -d "$1" ]] ; then
    echo "$1 is not a directory"
    exit
fi

source_dir=$(cd "$1"; pwd -P)
output_dir=""

if [[ -z "$2" || "$1" == "$2" ]] ; then
    echo "We directly update the original files"
elif [[ ! -d "$2" ]] ; then
    echo "$2 is not a directory"
    exit
elif [[ -d "$2" ]] ; then
    output_dir=$(cd "$2"; pwd -P)
fi

is_exiftool_exists=$(command -v exiftool)

if [[ -z "${is_exiftool_exists}" ]] ; then
    echo "The binary exiftool is missing. Please install it from there http://exiftool.org/"
    exit;
fi

# execute treatement on a file
# @arg filename
# @warning global variables modified:
function treat_file () {
    if [[ -z "$1" ]] ; then
        echo "Argument filename is missing for function treat_file()"
        exit
    fi
    local full_filename=$1

    echo -n -e "${full_filename}${terminal}\t"

    local img_dirname=$(dirname "$full_filename")
    local img_filename=$(basename "$full_filename")
    local img_filename_without_extension=$(echo "${img_filename}" | sed 's/\....$//')
    local cur_output_date_file_without_extension=""
    local year=""
    local monthday=""
    local time=""
    local prefix=""
    local suffix=""
    local comment=""
    local extension=""
    local month=""
    # quick filter to check if the year might be there
    local quick_regexp_filter='[1-2][0-9][0-9][0-9].*\.(....?)'
    local msg_date=""
    # prefix29-07-2007 23-03-59suffix.jpg
    local regexpreplace_a='^([^0-9]*)([0-3][0-9])[^0-9]([0-1][0-9])[^0-9]([1-2][0-9][0-9][0-9])[^0-9]?([0-2][0-9][^0-9][0-6][0-9][^0-9][0-9][0-9]){0,1}(.*)\.(....{0,1})$'
    # prefix20130101_174942suffix.jpg
    local regexpreplace_b='^([^0-9]*)([1-2][0-9][0-9][0-9])([0-1][0-9])([0-3][0-9])[_\.\ -]([0-2][0-9])([0-6][0-9])([0-9][0-9])(.*)\.(....{0,1})$'
    # prefix2020-12-23_20-10-23suffix.jpg
    local regexpreplace_c='^([^0-9]*)([1-2][0-9][0-9][0-9])[_\.\ -]([0-9][0-9][_\.\ -][0-9][0-9])[_\.\ -]?([0-9][0-9][_\.\ -][0-9][0-9][_\.\ -][0-9][0-9]){0,1}([^\.]*)\.(.*)$'
    # prefix2020suffix.jpg
    local regexpreplace_d='^([^0-9]*)([1-2][0-9][0-9][0-9])([^0-9][^\.]*)?\.(.*)$'
    # prefixsuffix.jpg
    local regexpreplace_e='^(.*)\.([^\.]*)$'

    if [[ "${img_filename}" =~ $regexpreplace_a ]] ; then
        prefix="${BASH_REMATCH[1]}"
        year="${BASH_REMATCH[4]}"
        monthday="${BASH_REMATCH[3]}-${BASH_REMATCH[2]}"
        time=$(echo "${BASH_REMATCH[5]}" | sed -e "s/[-\._]/:/g")
        suffix="${BASH_REMATCH[6]}"
        extension="${BASH_REMATCH[7]}"
        msg_date="${msg_date}Date found into the filename, "
    elif [[ "${img_filename}" =~ $regexpreplace_b ]] ; then
        prefix="${BASH_REMATCH[1]}"
        year="${BASH_REMATCH[2]}"
        monthday="${BASH_REMATCH[3]}-${BASH_REMATCH[4]}"
        time="${BASH_REMATCH[5]}:${BASH_REMATCH[6]}:${BASH_REMATCH[7]}"
        suffix="${BASH_REMATCH[8]}"
        extension="${BASH_REMATCH[9]}"
        msg_date="${msg_date}Date found into the filename, "
    elif [[ "${img_filename}" =~ $regexpreplace_c ]] ; then
        prefix="${BASH_REMATCH[1]}"
        year="${BASH_REMATCH[2]}"
        monthday=$(echo "${BASH_REMATCH[3]}" | sed -e "s/[\._:]/-/g")
        time=$(echo "${BASH_REMATCH[4]}" | sed -e "s/[-\._]/:/g")
        suffix="${BASH_REMATCH[5]}"
        extension="${BASH_REMATCH[6]}"
        msg_date="${msg_date}Date found into the filename, "
    elif [[ "${img_filename}" =~ $regexpreplace_d ]] ; then
        prefix="${BASH_REMATCH[1]}"
        year="${BASH_REMATCH[2]}"
        suffix="${BASH_REMATCH[3]}"
        extension="${BASH_REMATCH[4]}"
        msg_date="${msg_date}Year found into the filename, "
    elif [[ "${img_filename}" =~ $regexpreplace_e ]] ; then
        prefix=""
        year=""
        suffix=""
        extension="${BASH_REMATCH[2]}"
        msg_date="${msg_date}Date not found into the filename, "
    fi


    regexp="^(mp4|mov|avi|mkv|MP4|MOV|AVI|MKV)$"
    if [[ "${extension}" =~ $regexp ]] ; then
        isvideo="true"
    else
        isvideo="false"
    fi

    dir_parentname=$(basename "${img_dirname}")
    if [[ -z "${monthday}" || -z "${year}" ]] ; then
        # extract the date from the parent directory
        regexp='^([^0-9]*)([1-2][0-9][0-9][0-9])[_\.\ -]?([0-9][0-9])?[_\.\ -]?([0-9][0-9])?[_\.\ -]?([0-9][0-9][_\.\ -][0-9][0-9][_\.\ -][0-9][0-9]){0,1}(.*)$'
        if [[ "$dir_parentname" =~ $regexp && -z "${year}" ]]; then
            prefix="${BASH_REMATCH[1]}"
            year="${BASH_REMATCH[2]}"
            month="${BASH_REMATCH[3]}"
            day="${BASH_REMATCH[4]}"
            if [[ ! -z "${day}" ]] ; then
                monthday="${month}-${day}"
            fi
            time=$(echo "${BASH_REMATCH[5]}" | sed -e "s/[-\._]/:/g")
            suffix="${BASH_REMATCH[6]} ; ${img_filename_without_extension}"
            msg_date="${msg_date}Date found into the parent directory, "
        else

            # Check the parent directory for the month
            regexp="^([0-1][0-9])$"
            if [[ "$dir_parentname" =~ $regexp  && -z "${monthday}" ]]; then
                month="${BASH_REMATCH[1]}"
                msg_date="${msg_date}Month found into the parent directory, "
            fi

            # Check the grand-parent directory for the date
            dir_parentname=$(dirname "$img_dirname")
            dir_parentname=$(basename "$dir_parentname")
            regexp='^([^0-9]*)([1-2][0-9][0-9][0-9])([^0-9].*)?$'
            if [[ "$dir_parentname" =~ $regexp && -z "${year}" ]]; then
                prefix="${BASH_REMATCH[1]}"
                year="${BASH_REMATCH[2]}"
                if [[ -z "${BASH_REMATCH[3]}" ]] ; then
                    suffix="${img_filename_without_extension}"
                else
                    suffix="${BASH_REMATCH[3]} ; ${img_filename_without_extension}"
                fi
                msg_date="${msg_date}Date found into the grand-parent directory, "
            fi
        fi

        if [[ ! -z "${year}" && ! -z "${month}" && -z "${monthday}" ]] ; then
            monthday="${month}-01"
        fi

        if [[ -z "${year}" ]] ; then
            #echo -n -e "Date not found into filename or directories, "
            msg_date="Date not found into filename or directories, "
        fi
    else
        # we found the date into the filename so the parentname is added to the comment as prefix
            regexp="^[0-9]*$"
        if [[ ! "${dir_parentname}" =~ $regexp ]] ; then # no use if contains only number
            if [[ -z "${prefix}" ]] ; then
                prefix="${dir_parentname}"
            else
                prefix="${dir_parentname} ; ${prefix}"
            fi
        fi
    fi
    echo -n -e "${msg_date}${terminal}\t"

    # default value for month, day and time
    if [[ -z "${time}" ]] ; then
        time="01:01:01"
    fi
    if [[ -z "${monthday}" ]] ; then
        monthday="01-01"
    fi


    # the rest of the file is set into the comment
    comment=$(echo "${prefix} ${suffix}" | tr "_" " " | tr "-" " " | sed -e 's/^\ *//' -e 's/\ *$//')
    # define output filename
    cur_output_file="${full_filename}"
    if [[ ! -z "$output_dir" ]] ; then
        cur_output_file=$(echo "${full_filename}" | sed "s@${source_dir}@${output_dir}@")
    fi
    cur_output_file_without_extension=$(echo "${cur_output_file}" | sed 's/\....$//')
    # define date and new filename in case we rename the file
    img_filename_date=$(echo ${year}-${monthday}_${time} | sed 's/:/\./')
    cur_output_date_file_without_extension=$(echo "${cur_output_file}" | sed "s@${img_filename}@${img_filename_date}@")

    # Update exif data
    if [[ ! -z "${updatecommentonly}" ]] ; then
        if [[ ! -z "${comment}" ]] ; then
            # we update comment only
            if [[ ! -z "$output_dir" ]] ; then
                exiftool_output=$( exiftool -fast -m -Comment="${comment}" -Description="${comment}" -o "${cur_output_file}" "${full_filename}" 2>&1 )
            else
                exiftool_output=$( exiftool -fast -m -P -overwrite_original -Comment="${comment}" -Description="${comment}" "${full_filename}" 2>&1)
            fi
            exiftool_output=$(echo -n -e "${exiftool_output}" | tr -s "\n" "\t") # delete CR
            if [[ ${exiftool_output} =~ $re_success_exiftool ]] ; then
                echo -n -e "Success${terminal}\t"
                echo -n -e "comment updated${terminal}\t"
            else
                echo -n -e "Error${terminal}\t"
                echo -n -e "${terminal}\t"
            fi
            echo -n -e "${exiftool_output}${terminal}\t"
        fi
    elif [[ ! -z "${year}" ]] ; then
        # We update date and comment

        echo -n -e "${year}-${monthday} ${time}${terminal}\t"

        if [[ ! -z "$output_dir" ]] ; then
            # Modification by copy

            echo -n -e "${cur_output_file}${terminal}\t"
            echo -n -e "${comment}${terminal}\t"
            if [[ -z "${debug}" ]] ; then
                if [[ -z "${forceexifupdate}" ]] ; then
                    # if exists, we do not overwrite exif data
                    exiftool_output=$( exiftool -fast -m "-AllDates=${year}-${monthday} ${time}" -Comment="${comment}" -Description="${comment}" -o "${cur_output_file}" -if '(not $datetimeoriginal or ($datetimeoriginal eq "0000:00:00 00:00:00"))' "${full_filename}" 2>&1 )
                else
                    # Force exif exiftool DateTimeOriginal update?
                    exiftool_output=$( exiftool -fast -m "-AllDates=${year}-${monthday} ${time}" -Comment="${comment}" -Description="${comment}" -o "${cur_output_file}" "${full_filename}" 2>&1 )
                fi
                exiftool_output=$(echo -n -e "${exiftool_output}" | tr -s "\n" "\t") # delete CR
                if [[ ${exiftool_output} =~ $re_success_exiftool ]] ; then
                    echo -n -e "Success${terminal}\t"
                    echo -n -e "modification date updated${terminal}\t"
                    touch -amt $(echo "${year}-${monthday} ${time}" | sed -e "s/[-\ :]//g" -e "s/\([0-9][0-9]\)$/.\1/") "${cur_output_file}" # >/dev/null 2>&1
                elif [[ ${exiftool_output} =~ $re_uptodate_exiftool ]] ; then
                    echo -n -e "Up to date${terminal}\t"
                    regexp_filetype='\.(jpg|JPG|JPEG|jpeg)$'
                    if [[ ! "$full_filename" =~ $regexp_filetype ]]; then
                        echo -n -e "modification date updated${terminal}\t"
                        touch -amt $(echo "${year}-${monthday} ${time}" | sed -e "s/[-\ :]//g" -e "s/\([0-9][0-9]\)$/.\1/") "${cur_output_file}" # >/dev/null 2>&1
                    else
                        # no need to update modification date for jpeg file because the exif date will be used
                        echo -n -e "modification date unchanged${terminal}\t"
                        echo -n -e $(basename "${full_filename}")" has date${terminal}\t"
                    fi
                else
                    echo -n -e "Error${terminal}\t"
                    echo -n -e "${terminal}\t" # modification date
                    touch -amt  $(echo "${year}-${monthday} ${time}" | sed -e "s/[-\ :]//g" -e "s/\([0-9][0-9]\)$/.\1/") "${full_filename}" # >/dev/null 2>&1
                fi
                echo -n -e "${exiftool_output}${terminal}\t"
            else
                # debug mode
                updatedate="\"-AllDates=${year}-${monthday} ${time}\""
                ifnodate="-if '(not $datetimeoriginal or ($datetimeoriginal eq \"0000:00:00 00:00:00\"))'"
                if [[ ! -z "${forceexifupdate}" ]] ; then
                    # Force exif exiftool DateTimeOriginal update?
                    ifnodate=""
                fi
                echo -n -e exiftool -fast -m ${updatedate} -Comment=\"${comment}\" -Description=\"${comment}\" -o \"${cur_output_file}\" ${ifnodate} \"${full_filename}\""${terminal}\t"
            fi
        else
            # inplace modification

            echo -n -e "${terminal}\t" # no different output directory
            echo -n -e "${comment}${terminal}\t"
            if [[ -z "${debug}" ]] ; then

                if [[ -z "${forceexifupdate}" ]] ; then
                    # if exists, we do not overwrite exif data
                    exiftool_output=$( exiftool -fast -m -P -overwrite_original "-AllDates=${year}-${monthday} ${time}" -Comment="${comment}" -Description="${comment}" -if '(not $datetimeoriginal or ($datetimeoriginal e "0000:00:00 00:00:00"))' "${full_filename}" 2>&1)
                else
                    # Force exif exiftool DateTimeOriginal update?
                    exiftool_output=$( exiftool -fast -m -P -overwrite_original "-AllDates=${year}-${monthday} ${time}" -Comment="${comment}" -Description="${comment}" "${full_filename}" 2>&1)
                fi
                exiftool_output=$(echo -n -e "${exiftool_output}" | tr -s "\n" "\t") # delete CR
                if [[ ${exiftool_output} =~ $re_success_exiftool ]] ; then
                    echo -n -e "Success${terminal}\t"
                    echo -n -e "modification date updated${terminal}\t"
                    touch -amt  $(echo "${year}-${monthday} ${time}" | sed -e "s/[-\ :]//g" -e "s/\([0-9][0-9]\)$/.\1/") "${full_filename}" # >/dev/null 2>&1
                elif [[ ${exiftool_output} =~ $re_uptodate_exiftool ]] ; then
                    echo -n -e "Up to date${terminal}\t"
                    echo -n -e "modification date updated${terminal}\t"
                    touch -amt  $(echo "${year}-${monthday} ${time}" | sed -e "s/[-\ :]//g" -e "s/\([0-9][0-9]\)$/.\1/") "${full_filename}" # >/dev/null 2>&1
                else
                    echo -n -e "Error${terminal}\t"
                    echo -n -e "modification date updated${terminal}\t"
                    touch -amt  $(echo "${year}-${monthday} ${time}" | sed -e "s/[-\ :]//g" -e "s/\([0-9][0-9]\)$/.\1/") "${full_filename}" # >/dev/null 2>&1
                fi
                echo -n -e "${exiftool_output}${terminal}\t"
            else
                # debug mode
                updatedate="\"-AllDates=${year}-${monthday} ${time}\""
                ifnodate="-if '(not $datetimeoriginal or ($datetimeoriginal eq \"0000:00:00 00:00:00\"))'"
                if [[ ! -z "${forceexifupdate}" ]] ; then
                    # Force exif exiftool DateTimeOriginal update?
                    ifnodate=""
                fi

                echo -n -e exiftool -fast -m -P -overwrite_original ${updatedate} -Comment=\"${comment}\" -Description=\"${comment}\" ${ifnodate} \"${full_filename}\""${terminal}\t"
            fi
        fi

        # we rename the file
        if [[ "${isvideo}" == "true" && ! -z "${renamevideo}" || "${isvideo}" == "false" && ! -z "${renamephoto}" ]] ; then
            # rename file
            filename_unique=""
            if [[ ! -z "${addcomment}" && ! -z "${comment}" && ("${addcomment}" == "ALL" || ("${addcomment}" == "VIDEO" && "${isvideo}" == "true") || ("${addcomment}" == "PHOTO" && "${isvideo}" == "false" )) ]] ; then
                new_cur_output_file="${cur_output_date_file_without_extension} ${comment}.${extension}"
            else
                new_cur_output_file="${cur_output_date_file_without_extension}.${extension}"
            fi

            index=0
            while [[ -z "${filename_unique}" ]]  ; do
                if [[ ! -f "${new_cur_output_file}" ]] ; then
                    filename_unique="true"
                    if [[ -z "${debug}" ]] ; then
                        mv "${cur_output_file}" "${new_cur_output_file}"
                    else
                        echo -n -e "mv \"${cur_output_file}\" \"${new_cur_output_file}\"${terminal}\t"
                    fi
                    echo -n -e "${new_cur_output_file}${terminal}\t"
                else
                    # we add and suffix (integer) to the filename and check if it exists
                    index=$(($index + 1))
                    if [[ ! -z "${addcomment}" && ! -z "${comment}" && ("${addcomment}" == "ALL" || ("${addcomment}" == "VIDEO" && "${isvideo}" == true ) || ("${addcomment}" == "PHOTO" && "${isvideo}" == "false" )) ]] ; then
                        # we add comment if the option is activated for the specific extension
                        new_cur_output_file="${cur_output_date_file_without_extension}_${index} ${comment}.${extension}"
                    else
                        new_cur_output_file="${cur_output_date_file_without_extension}_${index}.${extension}"
                    fi
                fi
            done
        fi
    else
        # no date then no update
        if [[ -z "${terminal}" ]] ; then
            echo -n -e "\t\t\t"
        fi
        exiftool_output=$( exiftool -p '$filename has exif date anyway $dateTimeOriginal' "${full_filename}" 2>&1 )
        exiftool_output=$(echo -n -e "${exiftool_output}" | tr -s "\n" "\t") # delete CR
        if [[ ${exiftool_output} == *"Warning"* ]] ; then
            echo -n -e "date missing${terminal}\t"
        elif [[ ${exiftool_output} == *"has date"* ]] ; then
            echo -n -e "Up to date${terminal}\t"
        else
            echo -n -e "Error${terminal}\t"
        fi
        echo -n -e "modification date unchanged${terminal}\t"
        echo -n -e "${exiftool_output}${terminal}\t"
    fi
    echo ""

}

echo "Scan the directory : ${source_dir}"

echo "We analyse the directory to get the big picture of the job, please wait..."

nb_imgs=$(find -E "${source_dir}" -not -path '*/\.*' -iregex '.*\.(jpg|png|mp4|mov|avi|mkv)' -type f | wc -l | xargs)
echo -e "=> ${nb_imgs} files were found, That's the job!\n"

if [[ -z "${autoconfirm}" ]] ; then
    read -p "Are you sure you want to go on? [y/N]" -n 1 -r
    echo ""
    if [[ ! $REPLY =~ ^[Yy]$ ]] ; then
        echo "Bye!"
        exit
    fi
fi

if [[ ! -z "${forceexifupdate}" && -z "${updatecommentonly}" ]] ; then
    read -p "Are you sure you want to overwrite Exif datetimeoriginal? [y/N]" -n 1 -r
    echo ""
    if [[ ! $REPLY =~ ^[Yy]$ ]] ; then
        echo "Bye!"
        exit
    fi
fi

# echo header
echo -e "input file${terminal}\tdate source${terminal}\tdetected date${terminal}\tfile output${terminal}\tnote, comment${terminal}\tstatus${terminal}\tmodification date${terminal}\texiftool messages${terminal}"

# get the list of files without exif date and apply the process
find -E "${source_dir}" -not -path '*/\.*' -iregex '.*\.(jpg|png|mp4|mov|avi|mkv)' -type f | while read file; do treat_file "$file"; done

echo "End of the process."